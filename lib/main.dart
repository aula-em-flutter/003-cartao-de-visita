// ! Biblioteca
import 'package:flutter/material.dart';

// ! Função principal
void main() => runApp(const MeuApp());

// ! Classe "MeuApp"
class MeuApp extends StatelessWidget {
  // ! Inicialização da classe
  const MeuApp({super.key});

  // ! ...
  @override
  // ! ...
  Widget build(BuildContext context) {
    // ! Retorna a função "MaterialApp"
    return const MaterialApp(
      // ! Chama o widget "Scaffold"
      home: Scaffold(
        // ! Coloca uma cor de fundo na tela principal
        backgroundColor: Colors.blueGrey,
        // ! Chama o widget do corpo com uma área segura "SafeArea"
        body: SafeArea(
          // ! Chama um widget para centralização (Só permite um filho)
          child: Center(
            // ! Chama um widget para criação de coluna (Permite múltiplos filhos)
            child: Column(
              // ! Centraliza o conteúdo da coluna
              mainAxisAlignment: MainAxisAlignment.center,
              // ! ...
              children: (<Widget>[
                // ! Coloca um circulo para imagem
                CircleAvatar(
                  // ! Carrega uma imagem
                  backgroundImage: AssetImage('asset/image/Logo.jpg'),
                  // ! Tamanho do circulo desejado
                  radius: 75.0,
                ),
                // ! Chama um widget para texto
                Text(
                  'Renato Nunes',
                  // ! Permite estilizar o texto
                  style: TextStyle(
                    // ! Muda a fonte
                    fontFamily: 'American',
                    // ! Muda o tamanho da fonte
                    fontSize: 40.0,
                    // ! Coloca a fonte em negrito
                    fontWeight: FontWeight.bold,
                    // ! Coloca um espaçamento entre as letras
                    letterSpacing: 1.5,
                  ),
                ),
                // ! Chama um widget para texto
                Text(
                  'Engenheiro de Software',
                  style: TextStyle(
                    fontFamily: 'Play fair',
                    fontSize: 20.0,
                    letterSpacing: 2.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                // ! ...
                SizedBox(
                  height: 20.0,
                  width: 250.0,
                  // ! Cria uma linha de divisão
                  child: Divider(
                    color: Colors.white,
                  ),
                ),
                // ! Cria um card
                Card(
                  color: Colors.white,
                  elevation: 2.0,
                  shadowColor: Colors.black,
                  semanticContainer: true,
                  margin: EdgeInsets.symmetric(
                    vertical: 8.0,
                    horizontal: 30.0,
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: 10.0,
                        ),
                        Icon(
                          Icons.phone,
                          size: 25.0,
                          color: Colors.black,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          '+55 62 9 8150 5125',
                          style: TextStyle(
                              fontFamily: 'Play fair',
                              fontSize: 20.0,
                              color: Colors.black,
                              letterSpacing: 1.0),
                        )
                      ],
                    ),
                  ),
                ),
                Card(
                  color: Colors.white,
                  elevation: 2.0,
                  shadowColor: Colors.black,
                  semanticContainer: true,
                  margin: EdgeInsets.symmetric(
                    vertical: 0.5,
                    horizontal: 30.0,
                  ),
                  child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Row(
                      children: <Widget>[
                        SizedBox(
                          width: 10.0,
                        ),
                        Icon(
                          Icons.email,
                          size: 25.0,
                          color: Colors.black,
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          'renatusfn@outlook.com',
                          style: TextStyle(
                              fontFamily: 'Play fair',
                              fontSize: 20.0,
                              color: Colors.black,
                              letterSpacing: 1.0),
                        ),
                      ],
                    ),
                  ),
                ),
              ]),
            ),
          ),
        ),
      ),
    );
  }
}
